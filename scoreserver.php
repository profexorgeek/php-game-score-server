<?php

/* ==============================================================================
  Name:		XML Score Server API
  Summary:	API to save/return game scores
  Author:		Justin Johnson aka "profexorgeek", 2011
  Notes:		Self-contained class & helpers, in one file for portability

  SQL:
  CREATE TABLE IF NOT EXISTS `scores` (
  `ScoreID` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DeviceID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Score` bigint(20) NOT NULL,
  `Timestamp` int(11) NOT NULL,
  PRIMARY KEY (`ScoreID`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

  ============================================================================== */

//==============================================================================
// CONFIGURE THIS FOR YOUR ENVIRONMENT
//==============================================================================
$database['host'] = 'yourscoreserver.com';
$database['user'] = 'your_db_user';
$database['password'] = 'your_db_password!';
$database['database'] = 'your_db_name';

$secretKey = 'someSuperSecretKey';

// DO NOT SET TO TRUE IN PRODUCTION:
// disables hash verification and dumps query strings as comments!
define(DEBUG, true);

//==============================================================================
// MAIN SCORE SERVER CLASS
//==============================================================================
class ScoreServer {

	// properties
	private $action;
	private $key;
	private $response;
	private $converter;
	private $codes = array(
		0 => "Request successful",
		1 => "Existing score was higher.",
		2 => "No records found to delete",
		100 => "Player does not exist",
		101 => "Provided ID was non-unique",
		102 => "Failed to create player",
		103 => "Score was less than 1",
		104 => "No records found",
		200 => "Database connection failure",
		201 => "Generic query failure",
		202 => "Hash mismatch",
		203 => "Malformed request",
		300 => "Unknown failure",
	);

	public function __construct() {
		Debug::Trace("Initiating ScoreServer.");
		global $secretKey;
		$this->action = isset($_GET['RequestType']) ? $_GET['RequestType'] : null;
		$this->key = $secretKey;
		$this->Main();
	}

	// find request type and respond
	private function Main() {
		switch ($this->action) {
			case "Save" :
				$this->SaveScore();
				break;
			case "Delete" :
				$this->DeleteScore();
				break;
			case "Load" :
				$this->LoadScores();
				break;
			case "LoadRelative" :
				$this->GetRelativeRank();
				break;
			case "ScoreCount" :
				$this->GetScoreCount();
				break;
			default :
				$this->Response(203);
				break;
		}
	}

	// save submitted score
	private function SaveScore() {
		if (!defined(DEBUG) || DEBUG === false) {
			if (!$this->CheckHash()) {
				$this->Response(202);
				return false;
			}
		}
		if (
				isset($_GET['DeviceID']) && $_GET['DeviceID'] != '' &&
				isset($_GET['PlayerName']) && $_GET['PlayerName'] != '' &&
				isset($_GET['Score']) && $_GET['Score'] != ''
		) {
			// sanitize input
			$playerName = Database::Sanitize($_GET['PlayerName']);
			$deviceId = Database::Sanitize($_GET['DeviceID']);
			$score = Database::Sanitize($_GET['Score']);
			//$offset = (isset($_GET['Offset'])) ? Database::Sanitize($_GET['Offset']) : 0;
			$limit = (isset($_GET['Limit'])) ? Database::Sanitize($_GET['Limit']) : 10;
			$time = time();

			// does player have score already?
			$existingRecord = $this->GetPlayerHighScore($playerName, $deviceId);

			// no player on record, save new
			if ($existingRecord == false) {
				Debug::Trace("New player. Saving score.");
				$sql = "INSERT INTO scores(PlayerName, DeviceID, Score, Timestamp) VALUES('$playerName', '$deviceId',$score , $time)";
				$recordId = Database::WriteQuery($sql);
				$responseCode = 0;
			}
			// new score is higher, update existing
			elseif ($existingRecord['Score'] < $score) {
				Debug::Trace("Higher score, overwriting.");
				$scoreID = $existingRecord['ScoreID'];
				$sql = "REPLACE INTO scores(ScoreID, PlayerName, DeviceID, Score, Timestamp) VALUES($scoreID, '$playerName', '$deviceId',$score , $time)";
				$recordId = Database::WriteQuery($sql);
				$responseCode = 0;
			}
			// existing score is higher, do nothing
			else {
				Debug::Trace("Old score was same or better.");
				$recordId = $existingRecord['ScoreID'];
				$responseCode = 1;
			}

			// respond with data unless suppressed
			if ($recordId !== false) {
				if (isset($_GET['Response']) && $_GET['Response'] == false) {
					$this->Response($responseCode);
				} else {
					$relativeRanks = $this->GetRelativeRankData($recordId, $score, $limit);
					$this->Response($responseCode, $relativeRanks);
				}
			} else {
				$this->Response(104); // failed query
			}
		} else {
			$this->Response(203); // malformed
		}
	}

	// wipe a score from the database
	private function DeleteScore() {
		if (!defined(DEBUG) || DEBUG === false) {
			if (!$this->CheckHash()) {
				$this->Response(202);
				return false;
			}
		}

		if (
				isset($_GET['DeviceID']) && $_GET['DeviceID'] != '' &&
				isset($_GET['PlayerName']) && $_GET['PlayerName'] != ''
		) {
			$playerName = Database::Sanitize($_GET['PlayerName']);
			$deviceId = Database::Sanitize($_GET['DeviceID']);
			$delete = Database::WriteQuery("DELETE FROM scores WHERE PlayerName = '$playerName' && DeviceID = '$deviceId' LIMIT 1");
			if ($delete !== false) {
				if ($delete > 0) {
					$this->Response(0);
				} else {
					$this->Response(2);
				}
			} else {
				$this->Response(201);
			}
		} else {
			$this->Response(203);
		}
	}

	// return score data based on params
	private function LoadScores() {
		Debug::Trace("Loading scores.");
		$data = $this->GetScoreData();
		if ($data === false) {
			$this->Response(104);
		} else {
			$this->Response(0, $data);
		}
	}

	// return count of all scores in database
	private function GetScoreCount() {
		Debug::Trace("Getting Total Score Count");
		$results = Database::ReadQuery("SELECT COUNT(ScoreID) as ScoreCount FROM scores");
		if ($results) {
			$this->Response(0, $results);
		} else {
			$this->Response(104);
		}
	}

	// wrapper for GetRelativeRankData that sanitizes input and responds
	private function GetRelativeRank() {
		if (
				isset($_GET['PlayerName']) && $_GET['PlayerName'] != '' &&
				isset($_GET['DeviceID']) && $_GET['DeviceID'] != ''
		) {
			$playerName = Database::Sanitize($_GET['PlayerName']);
			$deviceId = Database::Sanitize($_GET['DeviceID']);
			$limit = (isset($_GET['Limit'])) ? Database::Sanitize($_GET['Limit']) : 10;
			$playerRecord = $this->GetPlayerHighScore($playerName, $deviceId);
			if ($playerRecord !== false) {
				$scoreData = $this->GetRelativeRankData($playerRecord['ScoreID'], $playerRecord['Score'], $limit);
				$this->Response(0, $scoreData);
			} else {
				$this->Response(104);
			}
		} else {
			$this->Response(203); // malformed
		}
	}

	// get position of a score in the list
	private function GetRelativeRankData($id, $score, $records) {
		Debug::Trace("Getting relative rank data.");

		// QueryZilla to get rank list
		$sql = "SELECT Rank FROM (
			SELECT @rownum:=@rownum + 1 as Rank, ScoreID
			FROM (SELECT @rownum:=0) r, scores
			ORDER BY Score DESC, Timestamp ASC
		) t WHERE ScoreID = $id";

		$idRank = Database::ReadQuery($sql);
		if ($idRank !== false) {
			$rank = $idRank[0]['Rank'];
			$limit = (isset($_GET['Limit'])) ? Database::Sanitize($_GET['Limit']) : 10;
			$offset = ($rank == 1) ? 0 : $rank - floor($limit / 2);
			if ($offset < 0) {
				$offset = 0;
			}

			// query for surrounding results since we know the rank
			$sql = "SELECT @ranknum:=@ranknum + 1 as Rank,
			PlayerName, Score, Timestamp,
			if(ScoreID = $id, 'true', 'false') as MostRecent
			FROM (SELECT @ranknum:=$offset) r, scores
			ORDER BY Score DESC, Timestamp ASC
			LIMIT $offset, $limit";

			$scores = Database::ReadQuery($sql);
			return $scores;
		} else {
			$this->Response(104); // no records
		}
	}

	// get N records from offset
	private function GetScoreData() {
		Debug::Trace("Getting highscores.");
		$limit = isset($_GET['Limit']) ? Database::Sanitize($_GET['Limit']) : 10;
		$offset = isset($_GET['Offset']) ? Database::Sanitize($_GET['Offset']) : 0;
		return Database::ReadQuery("SELECT * FROM scores ORDER BY Score DESC, Timestamp ASC LIMIT $offset, $limit");
	}

	// find a player's databased score
	private function GetPlayerHighScore($playerName, $deviceId) {
		Debug::Trace("Getting a player's high score data.");
		$sql = "SELECT * FROM scores WHERE PlayerName = '$playerName' AND DeviceID = '$deviceId' LIMIT 1";
		$results = Database::ReadQuery($sql);
		if ($results !== false) {
			return $results[0];
		}
		return false;
	}

	// create/output xml
	private function Response($code, $data = null) {
		$this->response .= DataConverter::SetToXml('responseCode', $code);
		$this->response .= DataConverter::SetToXml('responseString', $this->codes[$code]);

		// any code < 100 is success
		if ($code < 100 && $data != null) {
			Debug::Trace("We have a data response.");
			$responseData = DataConverter::QueryResultToXml($data, 'scoreRecord');
			$this->response .= DataConverter::SetToXml('responseData', $responseData);
		} else {
			Debug::Trace("Error code or null data supplied.");
		}

		$this->response = DataConverter::SetToXml('response', $this->response);

		Debug::Trace("Here comes the response:");
		print $this->response;
	}

	// ensure client/server hash match
	private function CheckHash() {
		// no hash? exit.
		Debug::Trace("Checking Hash.");
		if (isset($_GET['Hash'])) {
			$clientHash = $_GET['Hash'];
		} else {
			Debug::Trace("No Hash provided!");
			return false;
		}

		// break URL into chunks and alpha sort
		$params = array();
		$chunks = explode('&', $_SERVER['QUERY_STRING']);
		sort($chunks);
		for ($i = 0, $c = count($chunks); $i < $c; $i++) {
			$keyVal = explode('=', $chunks[$i]);
			if ($keyVal[0] !== 'Hash') {
				$params[$keyVal[0]] = $keyVal[1];
			}
		}

		// create reordered string for hashing
		$stringToHash;
		$count = 0;
		foreach ($array as $key => $value) {
			if ($count > 0) {
				$stringToHash .= "&";
			}
			$stringToHash .= "$key=$value";
			$count++;
		}

		// hash and check
		$serverHash = base64_encode(hash_hmac("sha256", $stringToHash, $this->key, TRUE));
		if ($serverHash == $clientHash) {
			Debug::Trace("Good Hash.");
			return true;
		} else {
			Debug::Trace("Bad Hash.");
			return false;
		}
	}

}

//==============================================================================
// HELPER CLASS FOR SERIALIZATION
//==============================================================================
class DataConverter {

	// convert key/value set to xml
	public static function SetToXml($key, $value) {
		return "<$key>$value</$key>";
	}

	// convert array to xml (recursive)
	public static function ArrayToXml($array) {
		$xmlString;

		if (!is_array($array)) {
			return;
		}

		foreach ($array as $key => $value) {
			$xmlString .= "<$key>";
			if (is_array($value)) {
				$xmlString .= self::ArrayToXml($value);
			} else {
				$xmlString .= $value;
			}
			$xmlString .= "</$key>";
		}
		return $xmlString;
	}

	// convert entire query result object to xml
	public static function QueryResultToXml($array, $label = 'row') {
		$xmlString;

		if (!is_array($array)) {
			return;
		}

		for ($i = 0; $i < count($array); $i++) {
			$xmlString .= self::SetToXml($label, self::ArrayToXml($array[$i]));
		}
		return $xmlString;
	}

}

//==============================================================================
// HELPER CLASS FOR DEBUG OUTPUT
//==============================================================================
class Debug {

	// prints commented output if DEBUG is true
	public static function Trace($output) {
		if (!defined(DEBUG) || DEBUG === false) {
			return;
		}
		print "<!-- ";
		if (is_array($output)) {
			print_r($output);
		} else {
			print $output;
		}
		print " -->\n";
	}

}

//==============================================================================
// HELPER CLASS FOR DATABASE ACCESS
//==============================================================================
class Database {

	private static $connection = false;
	private static $log = array();
	private static $queryCount = 0;
	private static $fatalError = false;
	private static $debug = false;

	// called before any request to make sure database connection is valid
	public static function Initialize() {
		global $database;

		if (isset(self::$connection) && self::$connection !== false) {
			unset($database);
			return;
		}

		if (defined(DEBUG) && DEBUG === true) {
			self::$debug = true;
		}

		Debug::Trace("Initializing Database.");
		self::$connection = mysql_connect($database['host'], $database['user'], $database['password']);
		if (self::$connection === false) {
			self::Error(true, 'Connection failed');
		}

		if (!mysql_select_db($database['database'])) {
			self::Error(true, 'Failed to select database.');
		}
		unset($database);
	}

	// takes array or variable and prevents mysql injection
	public static function Sanitize($userInput) {
		self::Initialize();
		Debug::Trace("Sanitizing: " . $userInput);

		if (is_numeric($userInput)) {
			$sanitized = $userInput;
		} else if (is_array($userInput)) {
			foreach ($userInput as $key) {
				$userInput[$key] = self::Sanitize($userInput[$key]);
			}
			$sanitized = $userInput;
		} else {
			$sanitized = mysql_real_escape_string($userInput);
		}

		return $sanitized;
	}

	// gets a key from mysql with low-probability of collision
	public static function SessionKey() {
		$id = self::ReadQuery("SELECT UUID() as sessionkey");
		if ($id !== false) {
			return $id[0]['sessionkey'];
		} else {
			return false;
		}
	}

	public static function LastInsertedRow() {
		self::Initialize();
		self::$queryCount++;
		return mysql_insert_id(self::$connection);
	}

	// execute a read query (select)
	public static function ReadQuery($sql) {
		self::Initialize();
		Debug::Trace("Read Query: " . $sql);
		self::$queryCount++;
		$result = mysql_query($sql, self::$connection) or self::Error(true, 'Read Query Fail');
		if ($result !== false && mysql_num_rows($result)) {
			$output;
			while ($row = mysql_fetch_assoc($result)) {
				$output[] = $row;
			}
			return $output;
		} else {
			self::Error(false, 'No results found.');
			return false;
		}
	}

	// execute a write query such as INSERT or DELETE
	public static function WriteQuery($sql) {
		self::Initialize();
		Debug::Trace("Write Query: " . $sql);
		self::$queryCount++;
		if (mysql_query($sql, self::$connection)) {
			if (strpos(strtolower($sql), 'delete') !== false) {
				$rowsAffected = mysql_affected_rows();
				return ($rowsAffected > -1) ? $rowsAffected : false;
			} else {
				return self::LastInsertedRow();
				;
			}
		} else {
			self::Error(true, 'Write query failed.');
			return false;
		}
	}

	// call this to log errors
	private static function Error($isCritical, $errorString) {
		if (self::$debug) {
			$errorString .= ' Error Message: ' . mysql_error();
		}
		if ($isCritical) {
			self::$log[] = $errorString;
			Debug::Trace(self::$log);
			die();
		} else {
			self::$log[] = $errorString;
		}
	}

	// get array of log messages
	public static function RetrieveLog() {
		return self::$log;
	}

}

//==============================================================================
// SELF-INSTANTIATE
//==============================================================================
$response = new ScoreServer();
?>